{For the title of this issue, use one of the following:
* Create a new {content_type} template (for new templates)
* Rework the {content_type} template (for template reworks)
* A title describing the task or decision needed (for style guide issues or minor template fixes)
}

Provide specific details about the template. You could adopt a user story approach to describe a new {content_type} template.  

_As a [type of user] I want [my goal] so that [my reason]._  

Example:
_As a developer, I want an installation template to document the install instructions for my application._ 

Keep the following points in mind while writing the description:
* If applicable, always include the relevant people using `@{Username}` who can support in resolving the issue. It can be someone who has previously worked on the template. Also, indicate the relevant Slack channel where the templateer can reach out to the larger community. 
* Use bullet points and subheadings to structure complex details instead of big paragraphs.
* Add links to any document references.


## Resources about this content type
{Optional: List any resources the templateer might consult while creating the template.}


## Want to work on this template?
Great! Make sure you follow our contributing guidelines:

1. Check that the template is unassigned. If it is assigned, you might be able to work with the current assignee as a paired writer.
2. Join The Good Docs Project [Slack workspace](https://thegooddocs.slack.com/). 
3. Read the [template contributing guide](https://gitlab.com/tgdp/templates/-/blob/main/CONTRIBUTING.md).
4. You are strongly encouraged to join one of the [working groups](https://thegooddocsproject.dev/working-group/) to get valuable support from the community such as mentorship, Git training, and helpful feedback as you contribute to your first template.
5. Request access to the `templates` repository by joining the #tech-requests channel in Slack and posting a request.
6. Assign yourself to an issue for the template that you want to work on.
7. Add the `Template phase:: Research` label to the issue.
8. Attend your template working group regularly to receive support and resources for your project.

We generally recommend composing your rough draft in Google Docs. For a list of reasons we recommend working Google Docs, see the [Research and draft the template](https://gitlab.com/tgdp/templates/-/blob/main/CONTRIBUTING.md#research-and-draft-the-template). That being said, you are not required to work in Google Docs. You can work in your preferred tool (such as Markdown, Microsoft Word, or Gitbooks). However, our experience has shown that Google Docs is the easiest tool for collaborating with other Good Docs Project contributors in the early phases of doc development.

When you have a rough draft ready, link to your Google Docs file here in the issue's comments and make sure you [set the General access permissions on your Google Doc](https://support.google.com/docs/answer/2494822?hl=en&ref_topic=4671185) to allow **Anyone with the link** to be a **Commenter**.